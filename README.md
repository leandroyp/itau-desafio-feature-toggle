# Desafio: Serviço de Feature Toggle

Este repositório contém o código de _kickstart_ do desafio de criação de um Serviço de Feature Toggle simples.

Mais instruções podem ser encontradas no arquivo `enunciado.md`.
